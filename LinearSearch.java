public class LinearSearch {
    public static <T> int search(T[] arr, int n, T x) {
        for (int i = 0; i < n; i++)
            if (x.equals(arr[i]))
                return i;
        return -1;
    }
}
